Source: wings3d
Maintainer: Debian Erlang Packagers <pkg-erlang-devel@lists.alioth.debian.org>
Uploaders: Sergei Golovan <sgolovan@debian.org>
Section: graphics
Priority: optional
Build-Depends: debhelper-compat (= 13), grep-dctrl,
 erlang-dev (>= 1:27), erlang-cl, erlang-wx,
 libgl-dev, libglu1-mesa-dev | libglu-dev, libjpeg-dev
Standards-Version: 4.7.0
Homepage: http://www.wings3d.com
Vcs-Browser: https://salsa.debian.org/erlang-team/packages/wings3d
Vcs-Git: https://salsa.debian.org/erlang-team/packages/wings3d.git


Package: wings3d
Architecture: any
Depends: ${erlang-base:Depends}, ${erlang-abi:Depends}, ${erlang-wx:Depends},
 ${erlang-xmerl:Depends}, erlang-cl,
 ${shlibs:Depends}, ${misc:Depends}
Suggests: ${erlang-dialyzer:Depends}, ${erlang-tools:Depends}, yafaray | aqsis
Description: Nendo-inspired 3D polygon mesh modeller
 Wings 3D is a polygon mesh modeller written entirely in Erlang.  The
 user interface was designed to be easy to use for both beginners and
 advanced users alike.  It was inspired by the famous Nendo modeller
 (from Izware.)
 .
 Unlike similar modelling programs (such as Blender), this program
 does not provide native support for doing animations (though you can
 output its models to an animation tool.)
 .
 Wings 3D supports the following import formats: Nendo (NDO),
 3D Studio (3DS), Wavefront (OBJ), and Adobe Illustrator 8 (AI).
 .
 Wings 3D supports the following export formats: Nendo (NDO),
 3D Studio (3DS), Wavefront (OBJ), VRML (WRL), Renderman (RIB),
 Hash:Animation Master (MDL), Renderware (RWX), Yafray, Toxic,
 and FBX via a third-party plug-in.
 .
 Open Source Erlang is a functional programming language designed at
 the Ericsson Computer Science Laboratory.
